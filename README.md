# peer60/haproxy-discover

haproxy combined with confd for HTTP load balancing

## Usage

Start the container making sure to expose port 80 on the host machine

```
docker run -e ETCD_HOST=172.17.42.1:4001 -p 80:80 peer60/haproxy-discover
```

Create at least one service inside of `/haproxy-discover/services`

```
etcdctl set "/haproxy-discover/services/myapp/domain" "myapp.com"
```

Add upstreams for the service

```
etcdctl set "/haproxy-discover/services/myapp/upstreams/one" "10.0.0.1:5000"
etcdctl set "/haproxy-discover/services/myapp/upstreams/two" "10.0.0.2:5001"
etcdctl set "/haproxy-discover/services/myapp/upstreams/three" "10.0.0.3:8001"
```

Hit your app and watch requests get balanced

```
curl myapp.com
```

## License - BSD

Copyright (c) 2014, peer60
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

